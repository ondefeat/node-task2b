import express from "express";
import cors from "cors";

const app = express();
app.use(cors());

app.get('/t2b', (req, res) => {
    const fio = req.query.fullname.toString()
        .trim().replace(/\s\s+/g, ' ');
    const wordsArray = fio.split(' ');
    if (wordsArray.length > 3 || fio == '' || /[0-9\/_]/.test(fio)) {
        res.send('Invalid fullname');
    } else if (wordsArray.length == 1) {
        res.send(wordsArray[0].toString());
    } else {
        var result = wordsArray.pop() + ' ';
        const initialArray = wordsArray.map(function (item) {
            return item.charAt(0) + '.';
        });
        result += initialArray.join(' ');
        res.send(result.toString());
    }
});

app.listen(3000, () => {
    console.log('Your app listening on port 3000!');
});
